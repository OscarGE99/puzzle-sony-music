var pieces = [
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/c973d8be-6a61-4b01-931a-b0be5601ae6d.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/61cc5bd0-0e8e-4349-9bd8-7e6e0b172070.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/a4a72fde-c7a2-44cf-9f10-d4092d38c191.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/a119a2b6-a8ef-4dd9-8be4-7217a897f509.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/9f430a75-9ce8-4ad0-b386-461dc21d5286.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/1a3d5075-bbaa-4b35-8bf0-e93cb48310ab.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/4dc1d13c-9456-4213-81cc-8cf0a45f250f.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/a6d9aefb-ca0f-4b4d-a084-90ae434dd022.png",
  "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/03b33fd7-c3ed-44a5-b7c6-61e07829fe06.png",
];
var take = new Audio("sounds/take.mp3");
var drop = new Audio("sounds/drop.mp3");
var win = new Audio("sounds/win.mp3");
var rows = 3;
var columns = 3;
var currTile;
var otherTile;
var dragImage;
var offsetX = 39.5,
  offsetY = 39.5;

var touchTimer;
var touchDuration = 500;
var dragMobile = false;

function showAlert() {
  var alert = document.getElementById("winMessage");
  alert.classList.remove("hidden");
  document.getElementById("pieces").style.display = "none";
  document.getElementById("arrow").style.display = "none";
  window.addEventListener("click", (event) => {
    // if (event.target !== alert) {
    //   alert.style.display = "none";
    // }
    alert.style.display = "none";
  });
}

function closeAlert() {
  var alert = document.getElementById("winMessage");
  alert.classList.add("hidden");
  initBoard();
  initPieces();
}

function initBoard() {
  document.getElementById("board").innerHTML = "";
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < columns; j++) {
      let tile = document.createElement("img");
      tile.src =
        "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png";
      //DRAG FUNCTIONALITY
      tile.addEventListener("dragstart", dragStart); //click on image to drag
      tile.addEventListener("dragover", dragOver); //drag an image
      tile.addEventListener("dragenter", dragEnter); //dragging an image into another one
      tile.addEventListener("dragleave", dragLeave); //dragging an image away from another one
      tile.addEventListener("drop", dragDrop); //drop an image onto another one
      tile.addEventListener("dragend", dragEnd); //after you completed dragDrop
      //TOUCH FUNCTIONALITY
      tile.addEventListener("touchstart", touchStart);
      tile.addEventListener("touchmove", touchMove);
      tile.addEventListener("touchend", touchEnd);

      document.getElementById("board").append(tile);
    }
  }
}
function initPieces() {
  document.getElementById("pieces").innerHTML = "";
  const shuffledPieces = shuffle(pieces);
  for (let i = 0; i < pieces.length; i++) {
    let tile = document.createElement("img");
    tile.src = shuffledPieces[i];
    tile.className = "piece";
    //DRAG FUNCTIONALITY
    tile.addEventListener("dragstart", dragStart); //click on image to drag
    tile.addEventListener("dragover", dragOver); //drag an image
    tile.addEventListener("dragenter", dragEnter); //dragging an image into another one
    tile.addEventListener("dragleave", dragLeave); //dragging an image away from another one
    tile.addEventListener("drop", dragDrop); //drop an image onto another one
    tile.addEventListener("dragend", dragEnd); //after you completed dragDrop
    //TOUCH FUNCTIONALITY
    tile.addEventListener("touchstart", touchStart);
    tile.addEventListener("touchmove", touchMove);
    tile.addEventListener("touchend", touchEnd);

    document.getElementById("pieces").append(tile);
  }
}

function shuffle(array) {
  let newArray = [...array];
  newArray.reverse();
  for (let i = newArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
  }
  return newArray;
}

function checkWin() {
  var board = document.getElementById("board");
  var piecesOrder = board.getElementsByTagName("img");
  for (var i = 0; i < piecesOrder.length; i++) {
    src = piecesOrder[i].getAttribute("src");
    if (src != pieces[i]) {
      break;
    }
    if (i == 8) {
      win.volume = 0.1;
      win.play();
      const jsConfetti = new JSConfetti();
      jsConfetti.addConfetti();
      
      let originalImage = document.createElement("img");
      originalImage.src =
        "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/b3565b6c-9446-4511-a570-f9c4d17b136c.png";
      originalImage.className = "originalImage";
      document.getElementById("board").innerHTML = "";
      board.append(originalImage);
      setTimeout(function () {
        showAlert();
      }, 50);
    }
  }
}

function dragStart() {
  currTile = null;
  otherTile = null;
  this.style.opacity = "0";
  if (
    !this.src.includes(
      "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
    )
  ) {
    take.volume = 0.1;
    take.play();
  } else {
    this.style.opacity = "1";
  }
  currTile = this; //this refers to image that was clicked on for dragging
}

function dragOver(e) {
  e.preventDefault();
}

function dragEnter(e) {
  e.preventDefault();
}

function dragLeave() {}

function dragDrop() {
  if (
    !currTile.src.includes(
      "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
    )
  ) {
    drop.volume = 0.1;
    drop.play();
  }
  otherTile = this; //this refers to image that is being dropped on
}

function dragEnd() {
  this.style.opacity = "1";
  if (
    currTile.src.includes(
      "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
    ) ||
    !otherTile
  ) {
    return;
  }
  let currImg = currTile.src;
  let otherImg = otherTile.src;
  currTile.src = otherImg;
  otherTile.src = currImg;
  currTile.className = "";
  otherTile.className = "piece";

  checkWin();
}

function touchStart(e) {
  currTile = null;
  otherTile = null;
  this.style.opacity = "0";
  dragImage.style.display = "block";
  if (
    !this.src.includes(
      "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
    )
  ) {
    take.volume = 0.1;
    take.play();
  } else {
    dragImage.style.display = "none";
    this.style.opacity = "1";
  }
  currTile = this;
  dragMobile = true;
  var touch = e.targetTouches[0];
  dragImage.src = currTile.src;
  if (window.innerWidth <= 620) {
    (offsetX = 24.5), (offsetY = 24.5);
  }
  dragImage.style.left = touch.clientX - offsetX + "px";
  dragImage.style.top = touch.clientY - offsetY + "px";
}

function touchMove(e) {
  e.preventDefault();
  if (
    this.src.includes(
      "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
    )
  ) {
    dragImage.style.display = "none";
  }
  var touch = e.targetTouches[0];
  dragImage.style.left = touch.clientX - offsetX + "px";
  dragImage.style.top = touch.clientY - offsetY + "px";
}

function touchEnd(e) {
  this.style.opacity = "1";
  if (touchTimer) {
    clearTimeout(touchTimer);
  }

  if (dragMobile) {
    e.preventDefault();
    e.stopPropagation();
    var changedTouch = e.changedTouches[0];
    const touchElement = document.elementFromPoint(
      changedTouch.clientX,
      changedTouch.clientY
    );
    if (
      !touchElement.src ||
      (!touchElement.src.includes(
        "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
      ) &&
        !pieces.some(function (piece) {
          return piece == touchElement.src;
        }))
    ) {
      dragImage.style.display = "none";
      return;
    }
    otherTile = touchElement;
    if (
      !currTile.src.includes(
        "https://image.emails.sonymusicfans.com/lib/fe9212747567007476/m/1/ad15917b-a8a4-4e86-a8b4-7295af8b7196.png"
      ) ||
      !otherTile
    ) {
      drop.volume = 0.1;
      drop.play();
    } else {
      return;
    }
    let currImg = currTile.src;
    let otherImg = otherTile.src;
    currTile.src = otherImg;
    otherTile.src = currImg;
    currTile.className = "";
    otherTile.className = "piece";
    dragImage.style.display = "none";

    checkWin();
  }
}

window.onload = function () {
  initBoard();
  initPieces();
  dragImage = document.getElementById("dragImage");
};
